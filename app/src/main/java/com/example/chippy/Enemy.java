package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.Image;

import java.util.ArrayList;

public class Enemy {
    private Bitmap image;
    private Rect hitbox;

    private int xPosition;
    private int yPosition;
    Bullets enemyBullets;




    public Enemy(Context context, int x, int y) {
        // 1. set up the initial position of the Enemy
        this.xPosition = x;
        this.yPosition = y;

        // 2. Set the default image - all enemies have same image
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.villian);

        // 3. Set the default hitbox - all enemies have same hitbox
        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.image.getWidth(),
                this.yPosition + this.image.getHeight()
        );


        enemyBullets = new Bullets();
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }
    // Make a new bullet
    public void spawnBulletEnemy() {
        // make bullet come out of middle of enemy
        Rect bullet = new Rect(this.xPosition+this.image.getWidth()/2,
                this.yPosition + this.image.getHeight() / 2,
                this.xPosition + enemyBullets.getBulletWidth(),
                this.yPosition + this.image.getHeight() / 2 + enemyBullets.getBulletWidth()
        );
        enemyBullets.bulletsArrayEnemy.add(bullet);
    }
    public void updateHitboxEnemy() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.image.getWidth();
        this.hitbox.bottom = this.yPosition + this.image.getHeight();
    }


}
