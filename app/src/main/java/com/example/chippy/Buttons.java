package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Buttons {

    private Bitmap image;
    private Rect hitbox;

    private int xPositionButton;
    private int yPositionButton;
    Bullets bullets;


    public Buttons(Context context, int x, int y) {
        // 1. set up the initial position of the Enemy
        this.xPositionButton = x;
        this.yPositionButton = y;


        // 2. Set the default image - all enemies have same image
      //  this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ship);

        // 3. Set the default hitbox - all enemies have same hitbox
        this.hitbox = new Rect(
                this.xPositionButton,
                this.yPositionButton,
                this.xPositionButton + this.xPositionButton+60,
                this.yPositionButton + this.yPositionButton+60
        );


        bullets =  new Bullets();

    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public int getxPosition() {
        return xPositionButton;
    }

    public void setxPosition(int xPosition) {
        this.xPositionButton = xPosition;
    }

    public int getyPosition() {
        return yPositionButton;
    }

    public void setyPosition(int yPosition) {
        this.yPositionButton = yPosition;
    }

}

