package com.example.chippy;

import android.graphics.Rect;

import java.util.ArrayList;

public class Bullets {



    public ArrayList<Rect> bulletsArrayPlayer = new ArrayList<Rect>();
    public final int BULLET_WIDTH = 15;

    public ArrayList<Rect> bulletsArrayEnemy = new ArrayList<Rect>();
    public final int BULLET_WIDTH1 = 10;


    public ArrayList<Rect> bulletsArrayWorldObstackles = new ArrayList<Rect>();

    public Bullets(){

        for (int i=0; i < 8;i++){
            this.bulletsArrayWorldObstackles.add(new Rect(20,
                    60+i*60,
                    60,
                    100+i*60
            ));
        }


    }



    public ArrayList<Rect> getBulletsPlayer() {
        return bulletsArrayPlayer;
    }
    public ArrayList<Rect> getBulletsEnemy() {
        return bulletsArrayEnemy;
    }

    public void setBulletsPlayer(ArrayList<Rect> bullets) {
        this.bulletsArrayPlayer = bullets;
    }

    public void setBulletsEnemy(ArrayList<Rect> bullets) {
        this.bulletsArrayEnemy = bullets;
    }


    public int getBulletWidth() {
        return BULLET_WIDTH;
    }







    public void spawnBullet() {
        // make bullet come out of middle of enemy
        for (int i=0; i <= 5;i++){
            Rect bullet = new Rect(0,
                    i+10,
                    40+i,
                    40+i
            );
            bulletsArrayEnemy.add(bullet);
        }
    }



}
