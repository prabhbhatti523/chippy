
package com.example.chippy;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

import android.media.Image;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GameEngine extends SurfaceView implements Runnable{

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG="PONG-GAME";
    boolean btPress = false;
    String buttonPressedByPlayer = "";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;
    int numLoops = 0;
    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // setting up timers to calculate elapsed timr
//    Timer T=new Timer();
//    long startTime = 0;
//    long endTime = 0;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    Player player;
    Enemy enemy;

    // buutons for player movement
    Buttons leftButton;
    Buttons rightButton;
    Buttons topButton;
    Buttons bottomButton;

    // for diagonal movement
    Buttons leftUpButton;
    Buttons rightUpButton;
    Buttons rightDownButton;
    Buttons leftDownButton;

    int buttonWidthAndHeight = 100;
    // power up button
    Buttons powerUpButton;

    Bitmap imageRightUp;
    Bitmap imageLeftUp;
    Bitmap imageRightDown;
    Bitmap imageLeftDown;

    // hits for enemy core to kill enemy
    int enemyCoreHits = 0;

    Bitmap background;
    int bgYPosition = 0;
    int backgroundBottomSide = 0;

    float touchX;
    float touchY;
    Bullets bullets;


   int score = 0;
   int lives = 10;
   int powerUpTime = 0;

   // labels
   String playerWonlabel = "";
    String playerLostlabel = "";
    String restartGamelabel = "";

    ArrayList<Enemy> branches = new ArrayList<>();
    ArrayList<Enemy> branchesSaved = new ArrayList<>();

    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();
       // this.startTime = System.currentTimeMillis();
        this.screenWidth = w;
        this.screenHeight = h;

        bullets = new Bullets();

        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites
            this.player = new Player(getContext(),400,600);

            this.enemy = new Enemy(getContext(),w/2,h/3);

            branches.add(enemy);
        //  horizontal branche
        Enemy branch1 = new Enemy(getContext(),enemy.getxPosition(),enemy.getyPosition()-30);
        branch1.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyh));
        branches.add(branch1);

        Enemy branch2 = new Enemy(getContext(),enemy.getxPosition()-70,enemy.getyPosition()+enemy.getImage().getHeight());
        branch2.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyh));
        branches.add(branch2);
        //verticle branch
        Enemy branch3 = new Enemy(getContext(),enemy.getxPosition()+enemy.getImage().getWidth(),enemy.getyPosition());
        branch3.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyv));
        branches.add(branch3);

        Enemy branch4 = new Enemy(getContext(),enemy.getxPosition()-30,branch2.getyPosition()-branch2.getImage().getWidth());
        branch4.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyv));
        branches.add(branch4);

        Enemy branch5 = new Enemy(getContext(),enemy.getxPosition()-branch2.getImage().getWidth(),branch2.getyPosition()-branch2.getImage().getWidth()-branch2.getImage().getHeight());
        branch5.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyh));
        branches.add(branch5);

        Enemy branch6 = new Enemy(getContext(),enemy.getxPosition()+branch2.getImage().getWidth(),enemy.getyPosition()-branch3.getImage().getHeight());
        branch6.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyv));
        branches.add(branch6);

        Enemy branch7 = new Enemy(getContext(),branch3.getxPosition(),branch3.getyPosition()+branch3.getImage().getHeight());
        branch7.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyh));
        branches.add(branch7);

        Enemy branch8 = new Enemy(getContext(),branch2.getxPosition()-branch2.getImage().getHeight(),branch2.getyPosition());
        branch8.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyv));
        branches.add(branch8);

        Enemy branch9 = new Enemy(getContext(),branch1.getxPosition(),branch1.getyPosition()-branch1.getImage().getHeight());
        branch9.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyh));
        branches.add(branch9);

        Enemy branch10 = new Enemy(getContext(),branch3.getxPosition()+branch3.getImage().getWidth(),branch3.getyPosition());
        branch10.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyv));
        branches.add(branch10);

        Enemy branch11 = new Enemy(getContext(),branch2.getxPosition(),branch2.getyPosition()+branch2.getImage().getHeight());
        branch11.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyh));
        branches.add(branch11);

        Enemy branch12 = new Enemy(getContext(),branch4.getxPosition()-branch4.getImage().getWidth(),branch4.getyPosition());
        branch12.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.enemyv));
        branches.add(branch12);

        // all enemy branches are saved into another array
        this.branchesSaved.addAll(branches);

        this.leftButton = new Buttons(getContext(),40,660);
        this.rightButton = new Buttons(getContext(),260,660);
        this.topButton = new Buttons(getContext(),150,550);
        this.bottomButton = new Buttons(getContext(),150,770);


        // diagonal movement buttons
        this.leftUpButton = new Buttons(getContext(),40,550);
        this.rightUpButton = new Buttons(getContext(),260,550);
        this.rightDownButton = new Buttons(getContext(),260,770);
        this.leftDownButton = new Buttons(getContext(),40,770);

        // power up button
        this.powerUpButton = new Buttons(getContext(),1900,20);


        // @TODO: Any other game setup stuff goes here

        // setup the background
        this.background = BitmapFactory.decodeResource(context.getResources(), R.drawable.background2);
        // dynamically resize the background to fit the device
        this.background = Bitmap.createScaledBitmap(
                this.background,
                this.screenWidth,
                this.screenHeight,
                false
        );



        this.bgYPosition = 0;

        //rotate image

        Matrix matrix1 = new Matrix();
        Bitmap myImg1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.shipup);
        matrix1.preRotate(45);
        Bitmap rotated = Bitmap.createBitmap(myImg1, 0, 0, myImg1.getWidth(), myImg1.getHeight(),matrix1, true);
        this.imageRightUp = rotated;

        Matrix matrix3 = new Matrix();
        Bitmap myImg3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.shipright);
        matrix3.preRotate(45);
        Bitmap rotated3 = Bitmap.createBitmap(myImg3, 0, 0, myImg3.getWidth(), myImg3.getHeight(),matrix3, true);
        this.imageRightDown = rotated3;

        Matrix matrix2 = new Matrix();
        Bitmap myImg2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.shipdown);
        matrix2.preRotate(45);
        Bitmap rotated2 = Bitmap.createBitmap(myImg2, 0, 0, myImg2.getWidth(), myImg2.getHeight(),matrix2, true);
        this.imageLeftDown = rotated2;


        Matrix matrix4 = new Matrix();
        Bitmap myImg4 = BitmapFactory.decodeResource(context.getResources(), R.drawable.shipleft);
        matrix4.preRotate(45);
        Bitmap rotated4 = Bitmap.createBitmap(myImg4, 0, 0, myImg4.getWidth(), myImg4.getHeight(),matrix4, true);
        this.imageLeftUp = rotated4;




    }

    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
            this.buttonPress(buttonPressedByPlayer);
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------


    public void moveBulletTowardsPlayer(int x, int y, int i) {



        // 1. calculate disatnce between enemy and player

        int a = (this.player.getxPosition() - x);
        int b = (this.player.getyPosition() - y);
        double distance = Math.sqrt((a * a) + (b * b));
        Log.d(TAG, "distance = " + distance);
        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);
        int angle = 15;

        // 3. move the bullet
        Rect bulletOfEnemy =enemy.enemyBullets.getBulletsEnemy().get(i);


        bulletOfEnemy.right = bulletOfEnemy.right +(int)(xn*angle);
        bulletOfEnemy.left = bulletOfEnemy.left +(int)(xn*angle);
        bulletOfEnemy.top = bulletOfEnemy.top +(int)(yn*angle);
        bulletOfEnemy.bottom = bulletOfEnemy.bottom +(int)(yn*angle);


    }
    public void moveBulletTowardsTap(int x, int y, int i) {



        // 1. calculate disatnce between tap and player
        int a = (x - this.player.getxPosition());
        int b = (y - this.player.getyPosition());
        double distance = Math.sqrt((a * a) + (b * b));
        Log.d(TAG, "distance = " + distance);
        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);
        int angle = 20;

        // 3. move the bullet

        Rect bulletOfPlayer =player.bullets.getBulletsPlayer().get(i);

        bulletOfPlayer.right = bulletOfPlayer.right +(int)(xn*angle);
        bulletOfPlayer.left = bulletOfPlayer.left +(int)(xn*angle);
        bulletOfPlayer.top = bulletOfPlayer.top +(int)(yn*angle);
        bulletOfPlayer.bottom = bulletOfPlayer.bottom +(int)(yn*angle);

    }



    int fireBulletsEnemy = 0;
    int moveEnemy = 1;
    // 1. Tell Android the (x,y) positions of your sprites
    public void updatePositions() {

        //if (this.lives > 0 && this.enemyCoreHits < 10) {
            if (this.lives >0 && this.enemyCoreHits < 10) {

                  if(this.powerUpTime <= 200) {
                    if(this.powerUpTime != 0){
                        this.powerUpTime = this.powerUpTime - 1;
                    }
                }


        // UPDATE BACKGROUND POSITION
        // 1. Move the background
        this.bgYPosition = this.bgYPosition - 10;


        backgroundBottomSide = this.bgYPosition + this.background.getHeight();
        // 2. Background collision detection
        if (backgroundBottomSide < 0) {
            this.bgYPosition = 0;
        }
        // @TODO: Update the position of the sprites

        // @TODO: Collision detection code

        //   movePlayer(this.mouseX,this.mouseY);

        fireBulletsEnemy = fireBulletsEnemy + 1;

        // enemy fire a bullet after every two seconds
        if (fireBulletsEnemy % 20 == 0) {
            this.score = this.score+1;

            for (int i = 0; i < branches.size(); i++) {

                if (moveEnemy >= 1 && moveEnemy <= 4) {

                    branches.get(i).setxPosition(branches.get(i).getxPosition() - 8);
                    branches.get(i).updateHitboxEnemy();

                } else if (moveEnemy >= 5 && moveEnemy <= 8) {

                    branches.get(i).setyPosition(branches.get(i).getyPosition() - 8);
                    branches.get(i).updateHitboxEnemy();

                } else if (moveEnemy >= 9 && moveEnemy <= 12) {

                    branches.get(i).setxPosition(branches.get(i).getxPosition() + 8);
                    branches.get(i).updateHitboxEnemy();


                } else {

                    branches.get(i).setyPosition(branches.get(i).getyPosition() + 8);
                    branches.get(i).updateHitboxEnemy();


                }

                branches.get(i).updateHitboxEnemy();

            }
            moveEnemy = moveEnemy + 1;
            if (moveEnemy == 17) {
                moveEnemy = 1;
            }

        }

        // spawn bullets world obstackles

                if(fireBulletsEnemy % 400 == 0){
                    for (int i=0; i < 8;i++){
                        this.bullets.bulletsArrayWorldObstackles.add(new Rect(20,
                                60+i*60,
                                60,
                                100+i*60
                        ));
                    }

                }

                for(int k=0; k< this.bullets.bulletsArrayWorldObstackles.size(); k++){

                     Rect bullet = this.bullets.bulletsArrayWorldObstackles.get(k);

                     bullet.right = bullet.right+10;
                     bullet.left = bullet.left+10;

                     if(bullet.intersect(player.getHitbox())){
                         this.lives = this.lives -1;
                         this.bullets.bulletsArrayWorldObstackles.remove(bullet);
                     }

                    if (bullet.right > screenWidth) {
                       // this.bullets.bulletsArrayWorldObstackles.remove(bullet);
                        this.bullets.bulletsArrayWorldObstackles.removeAll(this.bullets.bulletsArrayWorldObstackles);
                    }
                }


        if (fireBulletsEnemy % 50 == 0) {
            enemy.spawnBulletEnemy();



        }
        int BULLET_SPEED1 = 30;
        for (int i = 0; i < this.enemy.enemyBullets.getBulletsEnemy().size(); i++) {
            Log.d(TAG, "bullets" + this.enemy.enemyBullets.getBulletsEnemy().size());
            Rect bullet = this.enemy.enemyBullets.getBulletsEnemy().get(i);

            // function called to fire bullets towards player
            moveBulletTowardsPlayer(this.enemy.getxPosition(), this.enemy.getyPosition(), i);

        }
        // COLLISION DETECTION ON THE BULLET AND WALL
        for (int i = 0; i < this.enemy.enemyBullets.getBulletsEnemy().size(); i++) {
            Rect bullet = this.enemy.enemyBullets.getBulletsEnemy().get(i);

            if (player.getHitbox().intersect(bullet)) {
                if(this.powerUpTime > 0) {
                    this.enemy.enemyBullets.getBulletsEnemy().remove(bullet);

                }
                else {
                    lives = lives - 1;
                    this.enemy.enemyBullets.getBulletsEnemy().remove(bullet);
                }

            }

            // For each bullet, check if teh bullet touched the wall
            if (bullet.left < 0 || bullet.right > screenWidth || bullet.top < 0 || bullet.bottom > screenHeight) {
                this.enemy.enemyBullets.getBulletsEnemy().remove(bullet);
            }


        }

        // spawn bullets player
        numLoops = numLoops + 1;
        // DEAL WITH BULLETS
        // Shoot a bullet every (5) iterations of the loop
        if (numLoops % 15 == 0) {
            this.player.spawnBullet();
        }

      //  int BULLET_SPEED = 150;
        int bulletIndex;
        for (bulletIndex = 0; bulletIndex < this.player.bullets.getBulletsPlayer().size(); bulletIndex++) {
            Log.d(TAG, "bullets" + this.player.bullets.getBulletsPlayer().size());
            Rect bullet = this.player.bullets.getBulletsPlayer().get(bulletIndex);
//            bullet.top = bullet.top - BULLET_SPEED;
//            bullet.bottom = bullet.bottom - BULLET_SPEED;
            moveBulletTowardsTap((int) touchX, (int) touchY, bulletIndex);
        }
        // COLLISION DETECTION ON THE BULLET AND WALL
        for (int i = 0; i < this.player.bullets.getBulletsPlayer().size(); i++) {

            Rect bullet = this.player.bullets.getBulletsPlayer().get(i);

// if bullets hit the enemy branches , delete the branches
            if (bullet.intersect(branches.get(0).getHitbox())) {

                this.enemyCoreHits = enemyCoreHits + 1;

                this.player.bullets.getBulletsPlayer().remove(bullet);
                // 10 bullet hits are required for the core
                if (this.enemyCoreHits == 10) {
                    this.branches.remove(branches.get(0));
                    this.score = score+10;
                }


            }


            for (int j = 1; j < branches.size(); j++) {


                if (bullet.intersect(branches.get(j).getHitbox())) {
                    // if player bullet hit the benemy branch, remove the branch and increase score by 2
                    this.branches.remove(branches.get(j));
                    this.player.bullets.getBulletsPlayer().remove(bullet);
                    this.score = score+2;

                }
            }

            // For each bullet, check if teh bullet touched the wall
            if (bullet.left < 0 || bullet.right > screenWidth || bullet.top < 0 || bullet.bottom > screenHeight) {
                this.player.bullets.getBulletsPlayer().remove(bullet);
            }


        }
                // if player collides with the enemy or enemy branches, reset player position and decrease the player lives by 1

                for (int j = 0; j < branches.size(); j++) {
                    player.updateHitboxPlayer();


                    branches.get(j).updateHitboxEnemy();

                    if (player.getHitbox().intersect(branches.get(j).getHitbox())) {
                        player.setxPosition(400);
                        player.setyPosition(600);
                        this.lives = this.lives - 1;


                    }
                }


    }
        else{

            // when player won
            if(this.lives > 0){
                System.out.println("Player won");
               this.playerWonlabel = "YOU WON";
               this.restartGamelabel = "RESTART GAME";
                long tEnd = System.currentTimeMillis();

//                long tDelta = tEnd - 0;
//                double elapsedSeconds = tDelta / 1000.0+60;
//                score = (int)elapsedSeconds;


            }
            // when player lost

            else{
                System.out.println("Enemy Won");
                this.playerLostlabel = "YOU LOST";
                this.restartGamelabel = "RESTART GAME";

//               // final long endTime = SystemClock.elapsedRealtime();
//                long elapsedMilliSeconds = endTime - this.startTime;
//                double elapsedSeconds = elapsedMilliSeconds / 60000.0;
//                score = (int)elapsedSeconds;
//
//                Log.d("start time ", this.startTime+"");
//                Log.d("end time ", endTime+"");
            }
        }



    }

    public void buttonPress(String buttonPressedByPlayer){
        if(btPress){

            // player movements

            if(buttonPressedByPlayer == "left") {
                player.setImage(BitmapFactory.decodeResource(this.getResources(), R.drawable.shipleft));
                if(player.getxPosition() >= 0) {
                    player.setxPosition(player.getxPosition() - 30);
                    player.updateHitboxPlayer();
                }


            }
            if(buttonPressedByPlayer == "right") {
                player.setImage(BitmapFactory.decodeResource(this.getResources(), R.drawable.shipright));

                if(player.getxPosition()+player.getImage().getWidth() <= this.screenWidth) {
                    player.setxPosition(player.getxPosition() + 30);
                    player.updateHitboxPlayer();
                }

            }
            if(buttonPressedByPlayer == "top") {
                player.setImage(BitmapFactory.decodeResource(this.getResources(), R.drawable.shipup));

                if(player.getyPosition() >= 0) {
                    player.setyPosition(player.getyPosition() - 30);
                    player.updateHitboxPlayer();
                }
            }
            if(buttonPressedByPlayer == "bottom") {
                player.setImage(BitmapFactory.decodeResource(this.getResources(), R.drawable.shipdown));

                if(player.getyPosition()+player.getImage().getHeight() <= this.screenHeight-200) {
                    player.setyPosition(player.getyPosition() + 30);
                    player.updateHitboxPlayer();
                }
            }

            // diagonal movement
            if(buttonPressedByPlayer == "rightUp") {
                player.setImage(this.imageRightUp);
                if(player.getyPosition() >= 0 && player.getxPosition()+player.getImage().getWidth() <= this.screenWidth) {
                    player.setxPosition(player.getxPosition() + 30);
                    player.setyPosition(player.getyPosition() - 30);
                    player.updateHitboxPlayer();
                }
            }
            if(buttonPressedByPlayer == "rightDown") {
                player.setImage(this.imageRightDown);
                if(player.getyPosition()+player.getImage().getHeight() <= this.screenHeight-200 && player.getxPosition()+player.getImage().getWidth() <= this.screenWidth) {
                    player.setxPosition(player.getxPosition() + 30);
                    player.setyPosition(player.getyPosition() + 30);
                    player.updateHitboxPlayer();
                }
            }
            if(buttonPressedByPlayer == "leftUp") {

                player.setImage(this.imageLeftUp);
                if(player.getyPosition() >= 0 && player.getxPosition() >= 0) {
                    player.setxPosition(player.getxPosition() - 30);
                    player.setyPosition(player.getyPosition() - 30);
                    player.updateHitboxPlayer();
                }
            }
            if(buttonPressedByPlayer == "leftDown") {
                player.setImage(this.imageLeftDown);
                if(player.getxPosition() >= 0 && player.getyPosition()+player.getImage().getHeight() <= this.screenHeight-200) {
                    player.setxPosition(player.getxPosition() - 30);
                    player.setyPosition(player.getyPosition() + 30);
                    player.updateHitboxPlayer();
                }
            }

        }
        player.updateHitboxPlayer();
        enemy.updateHitboxEnemy();

    }


    // 2. Tell Android to DRAW the sprites at their positions
    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // Put all your drawing code in this section

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,0,0,255));
            paintbrush.setColor(Color.BLACK);
            // DRAW THE BACKGROUND
            // -----------------------------
            canvas.drawBitmap(this.background,
                    0,
                    this.bgYPosition,
                    paintbrush);

            canvas.drawBitmap(this.background,
                    0,
                    this.backgroundBottomSide,
                    paintbrush);


            //@TODO: Draw the sprites (rectangle, circle, etc)

            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(60);
          //  canvas.drawText("Score: 25", 20, 100, paintbrush);

            // drawing enemy and player
            canvas.drawBitmap(player.getImage(), player.getxPosition(),player.getyPosition(), paintbrush);
            player.updateHitboxPlayer();
//            canvas.drawBitmap(enemy.getImage(),enemy.getxPosition(),enemy.getyPosition(),paintbrush);
//            enemy.updateHitboxEnemy();
            paintbrush.setTextSize(60);
   paintbrush.setColor(Color.GREEN);
            for(int i=0;i<branches.size();i++){
                canvas.drawBitmap(branches.get(i).getImage(),branches.get(i).getxPosition(),branches.get(i).getyPosition(),paintbrush);
                branches.get(i).updateHitboxEnemy();

            }

            // draw bullet on screen player
            for (int i = 0; i < this.player.bullets.getBulletsPlayer().size(); i++) {
                Rect bullet = this.player.bullets.getBulletsPlayer().get(i);
                paintbrush.setColor(Color.RED);
                canvas.drawRect(bullet, paintbrush);
            }

            // draw bullet on screen enemy
            for (int i = 0; i < this.enemy.enemyBullets.getBulletsEnemy().size(); i++) {
                Rect bullet = this.enemy.enemyBullets.getBulletsEnemy().get(i);
                paintbrush.setColor(Color.RED);
                canvas.drawRect(bullet, paintbrush);
            }
            // draw bullet for world obstackles on screen enemy
            for (int i = 0; i < this.bullets.bulletsArrayWorldObstackles.size(); i++) {
                Rect bullet2 = this.bullets.bulletsArrayWorldObstackles.get(i);
                paintbrush.setColor(Color.RED);
                canvas.drawRect(bullet2, paintbrush);
            }



            canvas.drawText("SCORE: "+this.score+"",40,50,paintbrush);
            canvas.drawText("LIVES: "+this.lives+"",600,50,paintbrush);
            canvas.drawText("Power Up Time: "+this.powerUpTime+"",1200,50,paintbrush);
//            canvas.drawRect(120,620,180,680,paintbrush);
            canvas.drawText(""+this.playerWonlabel,400,450,paintbrush);
            canvas.drawText(""+this.playerLostlabel,400,450,paintbrush);
            canvas.drawText(""+this.restartGamelabel,700,800,paintbrush);




            paintbrush.setTextSize(40);
//            // draw buttons for player movement
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(leftButton.getxPosition(),leftButton.getyPosition(),leftButton.getxPosition()+buttonWidthAndHeight,leftButton.getyPosition()+buttonWidthAndHeight,paintbrush);
            paintbrush.setColor(Color.RED);
            canvas.drawText("L",leftButton.getxPosition()+10,leftButton.getyPosition()+50,paintbrush);




//            canvas.drawRect(40,700,100,760,paintbrush);
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(rightButton.getxPosition(),rightButton.getyPosition(),rightButton.getxPosition()+buttonWidthAndHeight,rightButton.getyPosition()+buttonWidthAndHeight,paintbrush);

            paintbrush.setColor(Color.RED);
            canvas.drawText("R",rightButton.getxPosition()+10,rightButton.getyPosition()+50,paintbrush);


//            canvas.drawRect(200,700,260,760,paintbrush);
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(topButton.getxPosition(),topButton.getyPosition(),topButton.getxPosition()+buttonWidthAndHeight,topButton.getyPosition()+buttonWidthAndHeight,paintbrush);

            paintbrush.setColor(Color.RED);
            canvas.drawText("U",topButton.getxPosition()+10,topButton.getyPosition()+50,paintbrush);

//            canvas.drawRect(120,780,180,840,paintbrush);
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(bottomButton.getxPosition(),bottomButton.getyPosition(),bottomButton.getxPosition()+buttonWidthAndHeight,bottomButton.getyPosition()+buttonWidthAndHeight,paintbrush);
            paintbrush.setColor(Color.RED);
            canvas.drawText("D",bottomButton.getxPosition()+10,bottomButton.getyPosition()+50,paintbrush);

            // diagonal buttons

            //left top button
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(leftUpButton.getxPosition(),leftUpButton.getyPosition(),leftUpButton.getxPosition()+buttonWidthAndHeight,leftUpButton.getyPosition()+buttonWidthAndHeight,paintbrush);
            paintbrush.setColor(Color.RED);
            canvas.drawText("LU",leftUpButton.getxPosition()+10,leftUpButton.getyPosition()+50,paintbrush);

            //right top button
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(rightUpButton.getxPosition(),rightUpButton.getyPosition(),rightUpButton.getxPosition()+buttonWidthAndHeight,rightUpButton.getyPosition()+buttonWidthAndHeight,paintbrush);
            paintbrush.setColor(Color.RED);
            canvas.drawText("RU",rightUpButton.getxPosition()+10,rightUpButton.getyPosition()+50,paintbrush);

            //right bottom button
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(rightDownButton.getxPosition(),rightDownButton.getyPosition(),rightDownButton.getxPosition()+buttonWidthAndHeight,rightDownButton.getyPosition()+buttonWidthAndHeight,paintbrush);
            paintbrush.setColor(Color.RED);
            canvas.drawText("RD",rightDownButton.getxPosition()+10,rightDownButton.getyPosition()+50,paintbrush);

            //left bottom button
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(leftDownButton.getxPosition(),leftDownButton.getyPosition(),leftDownButton.getxPosition()+buttonWidthAndHeight,leftDownButton.getyPosition()+buttonWidthAndHeight,paintbrush);
            paintbrush.setColor(Color.RED);
            canvas.drawText("LD",leftDownButton.getxPosition()+10,leftDownButton.getyPosition()+50,paintbrush);

            // power up button
            paintbrush.setColor(Color.GREEN);
            canvas.drawRect(powerUpButton.getxPosition(),powerUpButton.getyPosition(),powerUpButton.getxPosition()+buttonWidthAndHeight,powerUpButton.getyPosition()+buttonWidthAndHeight,paintbrush);
            paintbrush.setColor(Color.RED);
            canvas.drawText("PU",powerUpButton.getxPosition()+10,powerUpButton.getyPosition()+50,paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // Sets the frame rate of the game
    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

// when game restarted reset eveything
 public void resetEverything(){
       this.restartGamelabel = "";
     this.playerLostlabel = "";
     this.playerWonlabel = "";

        this.branches.removeAll(this.branches);

        this.branches.addAll(this.branchesSaved);

     for(int i=0;i<branches.size();i++){
         canvas.drawBitmap(branches.get(i).getImage(),branches.get(i).getxPosition(),branches.get(i).getyPosition(),paintbrush);
       //  branches.get(i).updateHitboxEnemy();

         this.lives = 10;
         this.enemyCoreHits = 0;

     }
     player.setxPosition(400);
     player.setyPosition(600);
     this.powerUpTime = 0;
     this.score = 0;

 }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();




        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {



            if ((event.getX() > leftButton.getxPosition()) && (event.getX() < leftButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > leftButton.getyPosition()) && (event.getY() < leftButton.getyPosition() + buttonWidthAndHeight)) {

                btPress = true;
                this.buttonPressedByPlayer = "left";


            }
            else  if ((event.getX() > rightButton.getxPosition()) && (event.getX() < rightButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > rightButton.getyPosition()) && (event.getY() < rightButton.getyPosition() + buttonWidthAndHeight)) {

                btPress = true;
                this.buttonPressedByPlayer = "right";


            }
            else  if ((event.getX() > topButton.getxPosition()) && (event.getX() < topButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > topButton.getyPosition()) && (event.getY() < topButton.getyPosition() + buttonWidthAndHeight)) {


                btPress = true;
                this.buttonPressedByPlayer = "top";


            }
            else if ((event.getX() > bottomButton.getxPosition()) && (event.getX() < bottomButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > bottomButton.getyPosition()) && (event.getY() < bottomButton.getyPosition() + buttonWidthAndHeight)) {


                btPress = true;
                this.buttonPressedByPlayer = "bottom";

            }

            // diagonal buttons

            else  if ((event.getX() > rightUpButton.getxPosition()) && (event.getX() < rightUpButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > rightUpButton.getyPosition()) && (event.getY() < rightUpButton.getyPosition() + buttonWidthAndHeight)) {


                btPress = true;
                this.buttonPressedByPlayer = "rightUp";


            }
            else  if ((event.getX() > rightDownButton.getxPosition()) && (event.getX() < rightDownButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > rightDownButton.getyPosition()) && (event.getY() < rightDownButton.getyPosition() + buttonWidthAndHeight)) {


                btPress = true;
                this.buttonPressedByPlayer = "rightDown";


            }
            else  if ((event.getX() > leftUpButton.getxPosition()) && (event.getX() < leftUpButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > leftUpButton.getyPosition()) && (event.getY() < leftUpButton.getyPosition() + buttonWidthAndHeight)) {


                btPress = true;
                this.buttonPressedByPlayer = "leftUp";


            }
            else  if ((event.getX() > leftDownButton.getxPosition()) && (event.getX() < leftDownButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > leftDownButton.getyPosition()) && (event.getY() < leftDownButton.getyPosition() + buttonWidthAndHeight)) {


                btPress = true;
                this.buttonPressedByPlayer = "leftDown";


            }
           else if(this.restartGamelabel == "RESTART GAME"){
               // this.redrawSprites();
                // reset everything called
                resetEverything();


            }
            else  if ((event.getX() > powerUpButton.getxPosition()) && (event.getX() < powerUpButton.getxPosition() + buttonWidthAndHeight) &&
                    (event.getY() > powerUpButton.getyPosition()) && (event.getY() < powerUpButton.getyPosition() + buttonWidthAndHeight)) {


                this.powerUpTime = 200;

            }
            else if((event.getX() > leftButton.getxPosition()+buttonWidthAndHeight) && (event.getX() < rightButton.getxPosition()) &&
                    (event.getY() > topButton.getyPosition()+buttonWidthAndHeight) && (event.getY() < bottomButton.getyPosition())){
                // do nothing if player touches area between the buttons
            }


            else {
                this.touchX = event.getX();
                this.touchY = event.getY();

            }

//            this.mouseX = (int)event.getX();
//            this.mouseY = (int)event.getY();
           // movePlayer(this.mouseX, this.mouseY);
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            btPress = false;
            // user lifted their finger

        }

        return true;
    }

}