package com.example.chippy;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

public class MainActivity extends AppCompatActivity {
    GameEngine chippy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        chippy = new GameEngine(this,size.x,size.y);

        setContentView(chippy);
    }

    @Override
    protected void onResume() {
        super.onResume();
        chippy.startGame();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        chippy.pauseGame();
    }
}
